import Header from "@/components/Header";
import Container from "@mui/material/Container";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import Head from "next/head";
import MKBox from "components/MKBox";

const Main = props => {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("lg"));
    return (
        <div>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width,initial-scale=1"
                    key="viewport"
                />
            </Head>
            <Header position="relative"></Header>
            <MKBox paddingTop="24px" component={"div"}>
                {props.children}
            </MKBox>
        </div>
    );
};

export { Main };
